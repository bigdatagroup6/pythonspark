#!/usr/bin/env python
import sys
import os
import json
import pyspark
from datetime import datetime

# VARIABLES
target = "ROTTERDAM"
targetLon = 4.256498
targetLat = 51.909849

# target = "SINGAPORE"
# targetLon = 103.84
# targetLat = 1.264

targetRange = 0.3

def stripper(json):
    result = dict()
    result['ts'] = json['ts']
    result['mmsi'] = json['mmsi']
    if ('destination' in json):
        result['destination'] = json['destination']
        result['eta_month'] = json['eta_month']
        result['eta_hour'] = json['eta_hour']
        result['eta_day'] = json['eta_day']
        result['eta_minute'] = json['eta_minute']
    else:
        result['lat'] = json['lat']
        result['lon'] = json['lon']
        result['lat'] = json['lat']
    return (result['mmsi'], result)

def calculateETA(line):
    try:
        arrivalDate = datetime.fromtimestamp(line[1][1]['ts'])
        eta_year = arrivalDate.year
        eta_month = line[1][0]['eta_month']
        eta_day = line[1][0]['eta_day']
        eta_hour = line[1][0]['eta_hour']
        eta_minute = line[1][0]['eta_minute']
        if eta_month > arrivalDate.month:
            eta_year = eta_year - 1
        eta = datetime(eta_year, eta_month, eta_day, eta_hour, eta_minute)

        return ((arrivalDate - eta).days, 1)
    except:
        return ("Unknown", 1)

# add actual job
def doJob(rdd):
    global target, targetLon, targetLat, targetRange
    jsonRdd = rdd.map(lambda line: json.loads(line))
    mmsisThatGoToTarget = jsonRdd.filter(lambda line: 'destination' in line and line['destination'] == target and line['eta_month'] > 0 and line['eta_day']  != 0 and line['eta_month'] < 13)\
        .groupBy(lambda line: line['mmsi'])\
        .keys()\
        .collect()
    shipsThatGoToTarget = jsonRdd.filter(lambda line: line['mmsi'] in mmsisThatGoToTarget)

    start = shipsThatGoToTarget.filter(lambda line: 'destination' in line and line['destination'] == target)\
        .map(stripper)\
        .reduceByKey(lambda a, b: [a, b][a['ts'] < b['ts']])

    end = shipsThatGoToTarget.filter(lambda line: 'lon' in line and pow(pow(abs(line['lon'] - targetLon), 2) + pow(abs(line['lat'] - targetLat), 2), 0.5) < targetRange)\
        .map(stripper)\
        .reduceByKey(lambda a, b: [a, b][a['ts'] < b['ts']])

    return start.join(end).map(calculateETA).reduceByKey(lambda a, b: a + b).sortByKey()

def main():
  # parse arguments
  in_dir, out_dir = sys.argv[1:]

  conf = pyspark.SparkConf().setAppName("countDestinations %s %s" % (in_dir, out_dir))
  sc = pyspark.SparkContext(conf=conf)

  # invoke job and put into output directory
  doJob(sc.textFile(in_dir)).coalesce(1).saveAsTextFile(out_dir)

if __name__ == '__main__':
  main()
