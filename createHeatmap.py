#!/usr/bin/env python
import sys
import os
import json
import pyspark

# target = "SINGAPORE"
target = "ROTTERDAM"

def format(line):
  return ("{0:.1f}".format(line['lat']) + "," + "{0:.1f}".format(line['lon']), 1)

def result(line):
  split = line[0].split(",")
  return '{"lat":' + split[0] + ',"lng":' + split[1] + ',"count":' + str(line[1]) + '},'

# add actual job
def doJob(rdd):
  global target
  jsonRdd = rdd.map(lambda line: json.loads(line))
  mmsisThatGoToTarget = jsonRdd.filter(lambda line: 'destination' in line and line['destination'] == target and line['eta_month'] > 0 and line['eta_day'] > 0 and line['eta_month'] < 13)\
      .groupBy(lambda line: line['mmsi'])\
      .keys()\
      .collect()
  return jsonRdd.filter(lambda line: line['mmsi'] in mmsisThatGoToTarget and 'lon' in line)\
      .map(format)\
      .reduceByKey(lambda a, b: a + b)\
      .sortBy(lambda a: a[1])\
      .map(result)\
      .coalesce(1)

def main():
  # parse arguments
  in_dir, out_dir = sys.argv[1:]

  conf = pyspark.SparkConf().setAppName("mapShipRoutes %s %s" % (in_dir, out_dir))
  sc = pyspark.SparkContext(conf=conf)

  # invoke job and put into output directory
  doJob(sc.textFile(in_dir)).saveAsTextFile(out_dir)


if __name__ == '__main__':
  main()
